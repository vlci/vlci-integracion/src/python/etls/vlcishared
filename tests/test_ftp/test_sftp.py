import time
import unittest
from unittest.mock import MagicMock, patch

from vlcishared.sftp.sftp import SFTPClient

file = 'fake_file.txt'
path_origin = '/fake/origin/path'
path_destiny = '/fake/destiny/path'
connection_aborted = 'Connection aborted'
connection_error = 'Connection error'
host = "fake_host"
username = "fake_user"
password = "fake_password"
port = 22


class TestSFTPClient(unittest.TestCase):
    def setUp(self):
        self.sftp_client = SFTPClient(
            host=host,
            username=username,
            password=password,
            port=port
        )

    def test_connect_successful(self):
        '''Test if pysftp.Connection
        is called with the correct parameters'''
        with patch("pysftp.Connection") as mock_connection:
            self.sftp_client.connect()

        mock_connection.assert_called_once_with(
            host=host,
            port=port,
            username=username,
            password=password,
            cnopts=self.sftp_client.cnopts
        )

    def test_connect_failure(self):
        '''Test that the correct exception is thrown in case of
        error'''
        with patch('pysftp.Connection',
                   side_effect=Exception(connection_error)):
            with self.assertRaises(ConnectionRefusedError) as context:
                self.sftp_client.connect()

        self.assertEqual(
            str(context.exception),
            f'Conexión fallida: {connection_error}'
        )

    def test_list_successful(self):
        '''Test that the files returned by pysft are given as result'''
        file_list = ['file1.txt', 'file2.txt', 'file3.txt']

        self.sftp_client.sftp = MagicMock()
        self.sftp_client.sftp.listdir = MagicMock(return_value=file_list)

        result = self.sftp_client.list(path_origin)

        self.sftp_client.sftp.listdir.assert_called_once()
        self.assertEqual(result, file_list)

    def test_list_sorted_date_modification(self):
        '''Test that the files returned by pysftp are sorted by modification date'''

        mock_file_1 = MagicMock()
        mock_file_1.filename = 'file1.txt'
        mock_file_1.st_mtime = time.time() - 10000

        mock_file_2 = MagicMock()
        mock_file_2.filename = 'file2.txt'
        mock_file_2.st_mtime = time.time() - 5000

        mock_file_3 = MagicMock()
        mock_file_3.filename = 'file3.txt'
        mock_file_3.st_mtime = time.time()

        self.sftp_client.sftp = MagicMock()
        self.sftp_client.sftp.listdir_attr = MagicMock(
            return_value=[mock_file_1, mock_file_2, mock_file_3])

        result = self.sftp_client.list_sorted_date_modification('/mock/path')

        expected_result = [
            ('file1.txt'),
            ('file2.txt'),
            ('file3.txt')
        ]

        # Assert that listdir_attr was called once with the correct path
        self.sftp_client.sftp.listdir_attr.assert_called_once_with()

        # Assert that the result matches the expected sorted list
        self.assertEqual(result, expected_result)

    def test_list_fail(self):
        '''Test that the correct exception is thrown when an error occurs
        while listing files'''
        directory = '/remote/path'
        self.sftp_client.sftp = MagicMock()
        self.sftp_client.sftp.listdir.side_effect = Exception(
            connection_aborted)

        with self.assertRaises(ConnectionAbortedError) as context:
            self.sftp_client.list(directory)

        self.assertEqual(
            str(context.exception),
            f'Fallo al listar los archivos: {connection_aborted}'
        )

    @patch('pysftp.Connection')
    def test_download_successful(self, mock_pysftp_connection):
        '''Test that the pysft get method is called
        with the correct parameters'''
        mock_get = MagicMock()
        mock_pysftp_connection.return_value.get = mock_get
        self.sftp_client.sftp = mock_pysftp_connection.return_value

        result = self.sftp_client.download(
            path_origin, path_destiny, file)

        mock_get.assert_called_once_with(
            f'{path_origin}/{file}',
            f'{path_destiny}/{file}'
        )
        self.assertIsNone(result)

    @patch('pysftp.Connection')
    def test_download_fail(self, mock_pysftp_connection):
        mock_pysftp_connection.return_value.get.side_effect = Exception(
            connection_aborted)
        self.sftp_client.sftp = mock_pysftp_connection.return_value

        with self.assertRaises(ConnectionAbortedError) as context:
            self.sftp_client.download(
                path_origin, path_destiny, file)

        self.assertEqual(
            str(context.exception),
            f'Descarga fallida: {connection_aborted}'
        )

    @patch('pysftp.Connection')
    def test_move_successful(self, mock_pysftp_connection):
        '''Test that the pysftp method rename is called with the correct
        parameters'''
        mock_rename = MagicMock()
        mock_pysftp_connection.return_value.rename = mock_rename
        self.sftp_client.sftp = mock_pysftp_connection.return_value

        result = self.sftp_client.move(
            path_origin, path_destiny, file)

        mock_rename.assert_called_once_with(
            f'{path_origin}/{file}',
            f'{path_destiny}/{file}'
        )
        self.assertIsNone(result)

    @patch('pysftp.Connection')
    def test_move_fail(self, mock_pysftp_connection):
        '''Test that the correct exception is thrown when an error occurs
        while moving files'''
        mock_rename = MagicMock()
        mock_pysftp_connection.return_value.rename = mock_rename
        mock_pysftp_connection.return_value.rename.side_effect = Exception(
            connection_aborted)
        self.sftp_client.sftp = mock_pysftp_connection.return_value

        with self.assertRaises(ConnectionAbortedError) as context:
            self.sftp_client.move(
                path_origin, path_destiny, file)

        self.assertEqual(
            str(context.exception),
            f'Fallo moviendo el fichero de {path_origin} a '
            f'{path_destiny}: {connection_aborted}'
        )

    @patch('pysftp.Connection')
    def test_upload_successful(self, mock_pysftp_connection):
        '''Test that the pysftp put method is called with the correct parameters'''
        mock_put = MagicMock()
        mock_pysftp_connection.return_value.put = mock_put
        self.sftp_client.sftp = mock_pysftp_connection.return_value

        local_file = 'local_file.txt'
        remote_path = '/remote/path'

        result = self.sftp_client.upload(local_file, remote_path)

        mock_put.assert_called_once_with(local_file, remote_path)
        self.assertIsNone(result)

    @patch('pysftp.Connection')
    def test_upload_fail(self, mock_pysftp_connection):
        '''Test that the correct exception is thrown when an error occurs while uploading'''
        mock_put = MagicMock()
        mock_pysftp_connection.return_value.put = mock_put
        mock_pysftp_connection.return_value.put.side_effect = Exception(
            connection_aborted)
        self.sftp_client.sftp = mock_pysftp_connection.return_value

        local_file = 'local_file.txt'
        remote_path = '/remote/path'

        with self.assertRaises(ConnectionAbortedError) as context:
            self.sftp_client.upload(local_file, remote_path)

        self.assertEqual(
            str(context.exception),
            f'Subida fallida: {connection_aborted}'
        )


if __name__ == "__main__":
    unittest.main()
