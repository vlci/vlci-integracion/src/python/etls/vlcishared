from unittest import TestCase
from vlcishared.env_variables.secrets import get_secret, InfisicalClientSingleton, SecretNotFoundError

class TestSecrets(TestCase):
    
    def test_get_secret(self):
        secret_name = "TEST"
        secret = get_secret(secret_name)
        self.assertEqual(secret, 'test')

    def test_get_secret_global(self):
        secret_name = "GLOBAL_UNIT_TEST_PURPOSE"
        secret = get_secret(secret_name)
        self.assertEqual(secret, 'test')

    def test_get_secret_not_found(self):
        secret_name = "NON_EXISTING_SECRET"
        with self.assertRaises(SecretNotFoundError):
            get_secret(secret_name)

    #Test para comprobar que únicamente se crea una instancia del cliente Infisical.
    def test_singleton_client(self):
        client_1 = InfisicalClientSingleton.get_client()
        client_2 = InfisicalClientSingleton.get_client()
        self.assertIs(client_1, client_2)

if __name__ == "__main__":
    unittest.main()
