from unittest import TestCase
from vlcishared.env_variables.config import get_config, ConfigNotFoundError

class TestConfig(TestCase):
    
    def test_get_config(self):
        variable_name = "TEST"
        config = get_config(variable_name)
        self.assertEqual(config, "test")

    def test_get_config_not_existing_variable(self):
        variable_name = "TEST1234"
        with self.assertRaises(ConfigNotFoundError):
            get_config(variable_name)

if __name__ == "__main__":
    unittest.main()