import unittest
from unittest.mock import Mock, patch

from vlcishared.flow.flow import FAILED_EXEC, SUCCESS_EXEC, FlowControl


class TestFlowControl(unittest.TestCase):
    def tearDown(self):
        """Restablecer el singleton antes de cada prueba para evitar interferencias entre pruebas"""
        if FlowControl._instance is not None:
            del FlowControl._instance 
        FlowControl._instance = None
    ''''
    def setUp(self):
        self.mock_mail = Mock()
        self.flow_control = FlowControl(mail=self.mock_mail)
    '''
    def test_handle_error_non_fatal(self):
        '''Este test asegura que cuando ocurre un error no fatal el estado
        de la ejecución es FALLO, no se llama a terminar ejecución de la ETL y
        tampoco se envía el correo'''
        self.mock_mail = Mock()
        self.flow_control = FlowControl(mail=self.mock_mail)

        self.flow_control.handle_error('Non-fatal error')

        self.assertEqual(self.flow_control.flow_state, FAILED_EXEC)
        self.mock_mail.append_line.assert_any_call(
            'Ejecución Fallida: ETL KO.')
        self.mock_mail.append_line.assert_any_call(
            'Causa del fallo: Non-fatal error')
        self.mock_mail.send.assert_not_called()

    def test_handle_error_fatal(self):
        '''Este test asegura que cuando ocurre un error fatal el estado de la 
        ejecución es FALLO, se llama a terminar ejecución de la ETL y
        se envía el correo'''
        self.mock_mail = Mock()
        self.flow_control = FlowControl(mail=self.mock_mail)

        with patch.object(self.flow_control, 'end_exec') as mock_end_exec:
            self.flow_control.handle_error('Fatal error', fatal=True)

            self.assertEqual(self.flow_control.flow_state, FAILED_EXEC)
            mock_end_exec.assert_called_once()

    def test_end_exec_success(self):
        '''Este test comprueba que en caso de ejecución exitosa no se envia
        correo y se llama a terminar el programa con código SUCCESS'''
        self.mock_mail = Mock()
        self.flow_control = FlowControl(mail=self.mock_mail)
        self.flow_control.flow_state = SUCCESS_EXEC

        with patch('sys.exit') as mock_exit:
            self.flow_control.end_exec()
            self.mock_mail.send.assert_not_called()
            mock_exit.assert_called_once_with(SUCCESS_EXEC)

    @patch('logging.getLogger')
    def test_end_exec_failure(self, mock_get_logger):
        '''Comprueba que cuando se termina una ejecución con fallo
        el código de salida es FALLO y se mandan los logs en el correo'''
        mock_mail = Mock()
        flow_control = FlowControl(mail=mock_mail)
        mock_logger = Mock()
        mock_handler = Mock()
        mock_handler.baseFilename = 'mock_logfile.log'
        mock_logger.handlers = [mock_handler]
        mock_get_logger.return_value = mock_logger
        flow_control.flow_state = FAILED_EXEC

        with patch('sys.exit') as mock_exit:
            flow_control.end_exec()
            mock_mail.append_line.assert_any_call(
                'Se adjuntan logs de la ejecución.')
            mock_mail.add_attachment.assert_called_once()
            mock_mail.send.assert_called_once()
            mock_exit.assert_called_once_with(FAILED_EXEC)

    @patch('logging.getLogger')
    def test_constructor_without_email(self, mock_get_logger):
        """Test that FlowControl works without passing an Email instance."""
        flow_control = FlowControl()  # No email passed
        mock_logger = Mock()
        mock_get_logger.return_value = mock_logger
        # Check initial flow state
        self.assertEqual(flow_control.flow_state, 0)
        self.assertIsNone(flow_control.mail)  # Check that mail is None

        # Simulate non-fatal error handling without email
        flow_control.handle_error("Non-fatal error")
        self.assertEqual(flow_control.flow_state, 1)  # Check error state

        # Check end_exec behavior with no mail and non-fatal error
        with self.assertRaises(SystemExit) as cm:
            flow_control.end_exec()
        self.assertEqual(cm.exception.code, 1)

    def test_singleton_instance(self):
        """Test that the FlowControl class follows the Singleton pattern."""

        flow_control1 = FlowControl()
        mock_mail = Mock()
        flow_control2 = FlowControl(mail=mock_mail)

        self.assertIs(flow_control1, flow_control2)


if __name__ == '__main__':
    unittest.main()
