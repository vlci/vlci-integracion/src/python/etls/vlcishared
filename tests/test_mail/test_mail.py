import unittest
from email.mime.application import MIMEApplication
from unittest.mock import patch

from vlcishared.mail.mail import Email


class TestEmail(unittest.TestCase):

    def setUp(self):
        self.email = Email('smtp.example.com', 587,
                           'user@example.com', 'password')

    def test_create_header(self):
        self.email.create_header(
            'user@example.com', 'User', 'recipient@example.com', 'Test Subject')
        self.assertEqual(self.email.sender, 'user@example.com')
        self.assertEqual(self.email.sender_name, 'User')
        self.assertEqual(self.email.to, 'recipient@example.com')
        self.assertEqual(self.email.subject, 'Test Subject')
        self.assertEqual(self.email.body, '')

    def test_append_line(self):
        self.email.create_header(
            'user@example.com', 'User', 'recipient@example.com', 'Test Subject')
        self.email.append_line('Line 1')
        self.email.append_line('Line 2')
        self.assertEqual(self.email.body, 'Line 1\nLine 2\n')

    def test_add_attachment(self):
        self.email.create_header(
            'user@example.com', 'User', 'recipient@example.com', 'Test Subject')
        self.email.add_attachment(
            'tests/test_mail/attachment/file.txt', subtype='plain')
        self.assertIsInstance(
            self.email.msg._payload[0], MIMEApplication)

    def test_send(self):
        # Mock smtplib.SMTP
        with patch('smtplib.SMTP') as mock_smtp:
            # Create the email message
            self.email.create_header(
                'user@example.com', 'User', 'recipient@example.com', 'Test Subject')
            self.email.append_line('This is the body')
            # Call the send method
            self.email.send()

            # Assert that smtplib.SMTP was called with the correct arguments
            mock_smtp.assert_called_once_with('smtp.example.com', 587)
            # Get the mock instance and call its methods
            smtp_instance = mock_smtp.return_value
            smtp_instance.starttls.assert_called_once()
            smtp_instance.login.assert_called_once_with(
                'user@example.com', 'password')
            smtp_instance.sendmail.assert_called_once_with(
                'user@example.com',
                'recipient@example.com',
                self.email.msg.as_string()
            )
            smtp_instance.quit.assert_called_once()


if __name__ == '__main__':
    unittest.main()
