import unittest
from unittest.mock import MagicMock, patch

import pandas as pd
from sqlalchemy import text
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session

from vlcishared.db.postgresql import PostgresConnector


class TestDbConnector(unittest.TestCase):

    def setUp(self):
        self.db_connector = PostgresConnector(
            host="localhost",
            port="0000",
            database="testdb",
            user="testuser",
            password="testpass"
        )

    @patch('vlcishared.db.postgresql.create_engine')
    @patch('vlcishared.db.postgresql.sessionmaker')
    def test_connect(self, mock_sessionmaker, mock_create_engine):
        mock_engine = MagicMock(spec=Engine)
        mock_session = MagicMock(spec=Session)

        mock_create_engine.return_value = mock_engine
        mock_sessionmaker.return_value.return_value = mock_session

        self.db_connector.connect()

        mock_create_engine.assert_called_once_with(
            'postgresql://testuser:testpass@localhost:0000/testdb')
        mock_sessionmaker.assert_called_once_with(bind=mock_engine)
        self.assertEqual(self.db_connector.engine, mock_engine)
        self.assertEqual(self.db_connector.session, mock_session)

    @patch('vlcishared.db.postgresql.create_engine')
    @patch('vlcishared.db.postgresql.sessionmaker')
    def test_close(self, mock_sessionmaker, mock_create_engine):
        mock_engine = MagicMock(spec=Engine)
        mock_session = MagicMock(spec=Session)

        mock_create_engine.return_value = mock_engine
        mock_sessionmaker.return_value.return_value = mock_session

        self.db_connector.connect()
        self.db_connector.close()

        mock_session.close.assert_called_once()
        mock_engine.dispose.assert_called_once()


if __name__ == "__main__":
    unittest.main()
