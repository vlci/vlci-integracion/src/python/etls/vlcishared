import unittest
from unittest.mock import MagicMock, patch

from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session

from vlcishared.db.oracle import OracleConnector


class TestDbConnector(unittest.TestCase):

    def setUp(self):
        self.db_connector = OracleConnector(
            host="localhost",
            port="0000",
            sid="testdb",
            user="testuser",
            password="testpass"
        )

    @patch('vlcishared.db.oracle.create_engine')
    @patch('vlcishared.db.oracle.sessionmaker')
    def test_connect(self, mock_sessionmaker, mock_create_engine):
        mock_engine = MagicMock(spec=Engine)
        mock_session = MagicMock(spec=Session)

        mock_create_engine.return_value = mock_engine
        mock_sessionmaker.return_value.return_value = mock_session

        self.db_connector.connect()

        mock_create_engine.assert_called_once_with(
            'oracle+oracledb://testuser:testpass@localhost:0000/testdb')
        mock_sessionmaker.assert_called_once_with(bind=mock_engine)
        self.assertEqual(self.db_connector.engine, mock_engine)
        self.assertEqual(self.db_connector.session, mock_session)

    @patch('vlcishared.db.oracle.create_engine')
    @patch('vlcishared.db.oracle.sessionmaker')
    def test_close(self, mock_sessionmaker, mock_create_engine):
        mock_engine = MagicMock(spec=Engine)
        mock_session = MagicMock(spec=Session)

        mock_create_engine.return_value = mock_engine
        mock_sessionmaker.return_value.return_value = mock_session

        self.db_connector.connect()
        self.db_connector.close()

        mock_session.close.assert_called_once()
        mock_engine.dispose.assert_called_once()

    @patch('vlcishared.db.oracle.create_engine')
    @patch('vlcishared.db.oracle.sessionmaker')
    def test_call_procedure(self, mock_sessionmaker, mock_create_engine):
        mock_engine = MagicMock(spec=Engine)
        mock_session = MagicMock(spec=Session)
        mock_result = MagicMock()

        mock_create_engine.return_value = mock_engine
        mock_sessionmaker.return_value.return_value = mock_session
        mock_session.execute.return_value = mock_result
        mock_result.fetchall.return_value = [('row1',), ('row2',)]

        self.db_connector.connect()
        result = self.db_connector.call_procedure(
            'test_procedure', 'param1', 'param2', is_function=True)

        mock_session.execute.assert_called_once_with(
            unittest.mock.ANY,  # Use ANY here to match the text object
            {'p0': 'param1', 'p1': 'param2'}
        )
        self.assertEqual(result, [('row1',), ('row2',)])


if __name__ == "__main__":
    unittest.main()
