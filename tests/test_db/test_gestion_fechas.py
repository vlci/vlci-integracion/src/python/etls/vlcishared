import logging
import unittest
from unittest.mock import MagicMock

from vlcishared.db.gestion_fechas import GestionFechas
from vlcishared.db.postgresql import PostgresConnector


class TestGestionFechas(unittest.TestCase):

    def setUp(self):
        self.db_connector = MagicMock(spec=PostgresConnector)
        self.gestion_fechas = GestionFechas(self.db_connector)
        logging.basicConfig(level=logging.INFO)

    def test_gestion_fechas_inicio_success(self):
        # Mock the call_procedure method for success case
        self.db_connector.call_procedure.side_effect = [
            # p_gf_averiguarejecucion returns a result indicating success
            [('t', 'success')],
            [('2023-07-02',)],  # p_gf_obtenerfecha returns a date
            None  # p_gf_registrarestadoetl does not return anything
        ]

        identifier = '136'
        param = 'fen'

        # Execute the method
        result = self.gestion_fechas.gestion_fechas_inicio(identifier, param)

        # Verify call_procedure calls
        self.db_connector.call_procedure.assert_any_call(
            'p_gf_averiguarejecucion', identifier, is_function=True)
        self.db_connector.call_procedure.assert_any_call(
            'p_gf_obtenerfecha', identifier, param, is_function=True)
        self.db_connector.call_procedure.assert_any_call(
            'p_gf_registrarestadoetl', identifier, 'EN PROCESO', is_function=True)

        # Verify the result
        self.assertEqual(result, '2023-07-02')

    def test_gestion_fechas_inicio_failure(self):
        # Mock the call_procedure method for failure case
        self.db_connector.call_procedure.side_effect = [
            [('t', 'f')]  # p_gf_averiguarejecucion returns a result indicating failure
        ]

        identifier = '136'
        param = 'fen'

        # Execute the method
        result = self.gestion_fechas.gestion_fechas_inicio(identifier, param)

        # Verify the result is ('t', 'f') due to failure indicator
        self.assertEqual(result, ('t', 'f'))

    def test_gestion_fechas_inicio_exception(self):
        # Mock the call_procedure method to raise an exception
        self.db_connector.call_procedure.side_effect = Exception(
            "Database error")

        identifier = '136'
        param = 'fen'

        # Execute the method
        result = None
        try:
            result = self.gestion_fechas.gestion_fechas_inicio(
                identifier, param)
        except Exception as e:
            self.assertEqual(str(e), "Database error")

        # Verify the result is None due to exception
        self.assertIsNone(result)

    def test_gestion_fechas_fin_success(self):
        # Mock the call_procedure method for success case
        self.db_connector.call_procedure.side_effect = [
            None,  # p_gf_registrarestadoetl does not return anything
            [('2024-07-07',)],  # p_gf_calcularNuevaFecha returns a date
        ]

        identifier = '136'
        param = 'fen'
        status = 'OK'

        # Execute the method
        result = self.gestion_fechas.gestion_fechas_fin(
            identifier, param, status)

        # Verify call_procedure calls
        self.db_connector.call_procedure.assert_any_call(
            'p_gf_registrarestadoetl', identifier, status, is_function=True)
        self.db_connector.call_procedure.assert_any_call(
            'p_gf_calcularNuevaFecha', identifier, is_function=True)

        # Verify the result
        self.assertEqual(result, '2024-07-07')

    def test_gestion_fechas_fin_failure(self):
        # Mock the call_procedure method to raise an exception
        self.db_connector.call_procedure.side_effect = Exception(
            "Database error")

        identifier = '136'
        param = 'fen'
        status = 'OK'

        # Execute the method
        result = None
        try:
            result = self.gestion_fechas.gestion_fechas_fin(
                identifier, param, status)
        except Exception as e:
            self.assertEqual(str(e), "Database error")

        # Verify the result is None due to exception
        self.assertIsNone(result)


if __name__ == "__main__":
    unittest.main()
