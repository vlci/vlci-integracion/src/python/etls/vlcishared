import unittest
from unittest.mock import MagicMock, patch

from pymongo import errors

from vlcishared.db.mongodb import MongoDBConnector


class TestMongoDBConnector(unittest.TestCase):

    def setUp(self):
        self.host = 'localhost'
        self.port = '27017'
        self.auth_source = 'admin'
        self.user = 'test_user'
        self.password = 'test_password'
        self.connector = MongoDBConnector(
            self.host, self.port, self.auth_source, self.user, self.password
        )

    @patch('vlcishared.db.mongodb.MongoClient')
    def test_get_db_connection_success(self, mock_mongo_client):
        """Prueba una conexión correcta."""
        mock_client_instance = MagicMock()
        mock_mongo_client.return_value = mock_client_instance
        db_instance = self.connector.get_db_connection()
        mock_mongo_client.assert_called_once_with(self.connector.uri)
        self.assertEqual(db_instance, mock_client_instance.get_database())

    @patch('vlcishared.db.mongodb.MongoClient')
    def test_get_db_connection_failure(self, mock_mongo_client):
        """Prueba que si hay un fallo de conexión se levanta una excepción."""
        mock_mongo_client.side_effect = errors.ConnectionFailure(
            'Connection failed')
        with self.assertRaises(ConnectionError) as context:
            self.connector.get_db_connection()
        self.assertIn(
            'Error de conexión a MongoDB: Connection failed', str(context.exception))

    @patch('vlcishared.db.mongodb.MongoClient')
    def test_disconnect(self, mock_mongo_client):
        """Prueba la desconexión exitosa con base de datos."""
        mock_client_instance = MagicMock()
        self.connector.client = mock_client_instance
        self.connector.disconnect()
        mock_client_instance.close.assert_called_once()

    def test_disconnect_no_client(self):
        """Test disconnect method when there is no client."""
        self.connector.client = None
        self.connector.disconnect()
        # Ensure that no exception is raised and no close method is called
        self.assertIsNone(self.connector.client)

    @patch('vlcishared.db.mongodb.MongoDBConnector.get_db_connection')
    @patch('vlcishared.db.mongodb.MongoDBConnector.disconnect')
    def test_insert_data_single_document(self, mock_disconnect, mock_get_db_connection):
        """Test inserting a single document into a MongoDB collection."""
        # Arrange
        connector = self.connector
        mock_db = MagicMock()
        mock_collection = MagicMock()
        mock_get_db_connection.return_value = mock_db
        mock_db.__getitem__.return_value = mock_collection

        test_data = {"name": "test", "value": 100}

        # Act
        connector.insert_data('test_collection', test_data)

        # Assert
        mock_collection.insert_one.assert_called_once_with(test_data)
        mock_disconnect.assert_called_once()

    @patch('vlcishared.db.mongodb.MongoDBConnector.get_db_connection')
    @patch('vlcishared.db.mongodb.MongoDBConnector.disconnect')
    def test_insert_data_multiple_documents(self, mock_disconnect, mock_get_db_connection):
        """Test inserting multiple documents into a MongoDB collection."""
        # Arrange
        connector = self.connector
        mock_db = MagicMock()
        mock_collection = MagicMock()
        mock_get_db_connection.return_value = mock_db
        mock_db.__getitem__.return_value = mock_collection

        test_data = [{"name": "test1", "value": 100},
                     {"name": "test2", "value": 200}]

        # Act
        connector.insert_data('test_collection', test_data)

        # Assert
        mock_collection.insert_many.assert_called_once_with(test_data)
        mock_disconnect.assert_called_once()

    @patch('vlcishared.db.mongodb.MongoDBConnector.get_db_connection')
    @patch('vlcishared.db.mongodb.MongoDBConnector.disconnect')
    def test_insert_data_raises_exception(self, mock_disconnect, mock_get_db_connection):
        """Test that insert_data raises an exception when a PyMongoError occurs."""
        # Arrange
        connector = self.connector

        mock_db = MagicMock()
        mock_collection = MagicMock()
        mock_get_db_connection.return_value = mock_db
        mock_db.__getitem__.return_value = mock_collection

        test_data = {"name": "test", "value": 100}

        mock_collection.insert_one.side_effect = errors.PyMongoError(
            "Insert failed")

        # Act & Assert
        with self.assertRaises(errors.PyMongoError):
            connector.insert_data('test_collection', test_data)

        mock_disconnect.assert_called_once()


if __name__ == '__main__':
    unittest.main()
