import unittest
from unittest.mock import Mock, patch

from vlcishared.utils.decorators import log_timer


class TestLogTimer(unittest.TestCase):

    @patch('time.time', side_effect=[100, 105])
    @patch('logging.getLogger')
    def test_log_timer(self, mock_get_logger, mock_time):
        '''Prueba que el return del decorator es el mismo de
        la funcion en la que se usa y que logea los segundos
        transcurridos de forma correcta'''
        mock_logger = Mock()
        mock_get_logger.return_value = mock_logger

        @log_timer
        def test_function():
            return "sample result"

        result = test_function()

        self.assertEqual(result, "sample result")
        mock_logger.info.assert_called_once_with(
            "La función test_function se ejecuto en 5 segundos"
        )


if __name__ == '__main__':
    unittest.main()
